function cacheFunction(cb) {
    // Should return a function that invokes `cb`.
    // A cache (object) should be kept in closure scope.
    // The cache should keep track of all arguments have been used to invoke this function.
    // If the returned function is invoked with arguments that it has already seen
    // then it should return the cached result and not invoke `cb` again.
    // `cb` should only ever be invoked once for a given set of arguments.
    let cacheObj = {};

    function returnFn(...arguments) {
    
        let wholeArguments = JSON.stringify(arguments);

        let returnedValue = cb(...arguments);
            
        if(wholeArguments in cacheObj) {
            console.log("returning from cache...")
            return cacheObj[wholeArguments];
        } else {
            console.log("Invoked cb...")
            cacheObj[wholeArguments] = returnedValue;
            return cacheObj[wholeArguments]
        }
    }
    return returnFn;
}
module.exports = cacheFunction;


