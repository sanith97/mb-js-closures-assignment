function limitFunctionCallCount(cb, n = 0) {
    // Should return a function that invokes `cb`.
    // The returned function should only allow `cb` to be invoked `n` times.
    // Returning null is acceptable if cb can't be returned

    let count = 0;
    function sendingFn(...parameters) {
        count++;
        let result = count > n ? null : cb(...parameters);
        return result;
    };

    return sendingFn;
}

module.exports = limitFunctionCallCount;






