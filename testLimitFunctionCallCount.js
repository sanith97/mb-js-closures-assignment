const result = require('./limitFunctionCallCount.js');

const closureFunction = result((a,b) => a + b, 2);

console.log(closureFunction(1,2));
console.log(closureFunction(4,1));
console.log(closureFunction(1,2));