const result = require('./cacheFunction.js');

const cachedSquare = result((num) => num * num);

console.log(cachedSquare(2)); 
console.log(cachedSquare(2)); 
console.log(cachedSquare(3)); 
console.log(cachedSquare(3)); 