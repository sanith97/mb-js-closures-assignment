function counterFactory(value = 0) {
    // Return an object that has two methods called `increment` and `decrement`.
    // `increment` should increment a counter variable in closure scope and return it.
    // `decrement` should decrement the counter variable and return it.

    let initialVal = value;
    return {
        increment: () => {
            initialVal++
            return initialVal
        },
        decrement: () => {
            initialVal--
            return initialVal
        },
    };
}

module.exports = counterFactory;
